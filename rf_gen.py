#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Naprej
# Generated: Thu May 14 21:34:45 2020
##################################################


if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import osmosdr
import time
import wx


class rf_gen(direction, grc_wxgui.top_block_gui):

    def __init__(self, direction):
        grc_wxgui.top_block_gui.__init__(self, title="Naprej")
        _icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
        self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 2000000
        self.duration = duration = 0.000367
        self.center = center = 40685500

        ##################################################
        # Blocks
        ##################################################
        self.osmosdr_sink_0 = osmosdr.sink( args="numchan=" + str(1) + " " + "hackrf" )
        self.osmosdr_sink_0.set_sample_rate(samp_rate)
        self.osmosdr_sink_0.set_center_freq(center, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(10, 0)
        self.osmosdr_sink_0.set_if_gain(20, 0)
        self.osmosdr_sink_0.set_bb_gain(20, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(0, 0)

        vec = self.generate_vector(direction)

        #self.blocks_vector_source_x_0 = blocks.vector_source_c((1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1 , 0, 1, 0, 1, 0, 1, 0, ), False, 1, [])
        self.blocks_vector_source_x_0 = blocks.vector_source_c(vec, False, 1, [])
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_gr_complex*1, int(samp_rate*duration))
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, 4500, 1, 0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_multiply_xx_0, 0), (self.osmosdr_sink_0, 0))
        self.connect((self.blocks_repeat_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_vector_source_x_0, 0), (self.blocks_repeat_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.osmosdr_sink_0.set_sample_rate(self.samp_rate)
        self.blocks_repeat_0.set_interpolation(int(self.samp_rate*self.duration))
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_duration(self):
        return self.duration

    def set_duration(self, duration):
        self.duration = duration
        self.blocks_repeat_0.set_interpolation(int(self.samp_rate*self.duration))

    def get_center(self):
        return self.center

    def set_center(self, center):
        self.center = center
        self.osmosdr_sink_0.set_center_freq(self.center, 0)

    def generate_vector(self, direction):
        dir_values =	{
        "fwd": 10,
        "fwd_l": 28,
        "fwd_r": 34,
        "back": 40,
        "left": 58,
        "right": 64
        } 
        # dir_num how many times does the pattern 1, 0 repeat
        dir_num = dir_values[direction]
        # four times 1, 1, 1, 0 repeated in front
        vec_len = dir_num + 16
        vec = []
        for i in range(4):
            vec.append(1)
            vec.append(1)
            vec.append(1)
            vec.append(0)
        for i in range(dir_num + 1):
            vec.append(1)
            vec.append(0)
        vec = vec * 10
        return tuple(vec)



def main(top_block_cls=rf_gen, options=None):

    tb = top_block_cls()
    tb.Start(True)
    tb.Wait()


if __name__ == '__main__':
    main()
