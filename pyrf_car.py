if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.wxgui import fftsink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import osmosdr
import time
import wx

from pynput import keyboard

from rf_gen import rf_gen
"""
from nazaj import nazaj
from levo import levo
from desno import desno
from naprej_levo import naprej_levo
from naprej_desno import naprej_desno
"""

def on_press(key):
    if key == keyboard.Key.esc:
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys
    if k in ['w', 'a', 's', 'd', 'q', 'e']:  # keys of interest
        # self.keys.append(k)  # store it in global-like variable
        if k == 'w':
            print("naprej")
            tb = rf_gen()
            tb.Start(True)
            tb.Wait()
            tb.Close()
        """
        if k == 'a':
            print("levo")
            tb = levo()
            tb.Start(True)
            tb.Wait()
        if k == 's':
            print("nazaj")
            tb = nazaj()
            tb.Start(True)
            tb.Wait()
        if k == 'd':
            print("desno")
            tb = desno()
            tb.Start(True)
            tb.Wait()
        if k == 'q':
            print("naprej_levo")
            tb = naprej_levo()
            tb.Start(True)
            tb.Wait()
        if k == 'e':
            print("naprej_desno")
            tb = naprej_desno()
            tb.Start(True)
            tb.Wait()
            """
    else:
        sys.exit()

def main(options=None):
    listener = keyboard.Listener(on_press=on_press)
    listener.start()  # start to listen on a separate thread
    listener.join()  # remove if main thread is polling self.keys
    """
    if smer == "w":
        tb = naprej()
        tb.Start(True)
        tb.Wait()
    """

if __name__ == '__main__':
    main()
